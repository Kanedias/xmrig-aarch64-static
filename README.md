Static binary builds of xmrig for aarch64, armeabi-v7a and x86_64 systems
=========================================================================

[![latest release](https://gitlab.com/Kanedias/xmrig-aarch64-static/-/badges/release.svg)](https://gitlab.com/Kanedias/xmrig-aarch64-static/-/releases)
[![pipeline status](https://gitlab.com/Kanedias/xmrig-aarch64-static/badges/master/pipeline.svg)](https://gitlab.com/Kanedias/xmrig-aarch64-static/-/commits/master)
[![upstream repo link](https://img.shields.io/badge/upstream%20repo-link-blue?style=flat&logo=github)](https://github.com/xmrig/xmrig)
[![repo license](https://img.shields.io/badge/license-GPLv3%2B-blue?style=flat&logo=gnu)](https://spdx.org/licenses/GPL-3.0-or-later.html)

Portable static x64 (x86_64), arm (armeabi-v7a) and arm64 (aarch64) binary builds for xmrig miner.
Suitable for bare Android, Termux, routers, restricted VMs or any other custom Linux installation.

Grab it from [**Releases**][0] page or use direct links below in automated download scripts:

* [**aarch64 (arm64) release binary**][1]
* [**armeabi-v7a (arm) release binary**][2]
* [**x86_64 (amd64) release binary**][3]

Disclaimer
----------

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

FAQ and Troubleshooting
-----------------------
* **Q:** Is this really a static build?  
  **A:** Yes, it doesn't depend on your system ld or libc or their versions at all.

* **Q:** Does [this][1] binary work on any aarch64 device?  
  **A:** Any device running Linux kernel and having aarch64 (arm64) instruction set.

* **Q:** Where's the source code of `xmrig` that you're building from?  
  **A:** I pull it from the official repository, you can find the whole build process [here](./.gitlab-ci.yml)

* **Q:** Can I use it on Raspberry Pi / Orange Pi?  
  **A:** Yes, it will work on any Linux distribution of your choice.

* **Q:** Can I use it in WSL on Windows?  
  **A:** Yes, WSL2 is a VM with real Linux kernel,
         so [this][2] binary should work out of the box without any changes.

* **Q:** Can I use it on Android?  
  **A:** Yes, you can use it in Termux or even on bare Android.

* **Q:** How do I use it on bare Android?  
  **A:** Every Android device has a special place `/data/local/tmp` where you can run test executables.
         Enable [ADB Debugging][4] on your phone, and then do the following:

```bash
host$ adb push xmrig-aarch64-static /data/local/tmp
host$ adb shell
device$ cd /data/local/tmp && chmod +x xmrig-aarch64-static
device$ ./xmrig-aarch64-static -u <your-wallet-address> -o <your.pool.ip.address:port>
```

* **Q:** My `xmrig` throws `Temporary failure in name resolution` error! What should I do?  
  **A:** This is a static build, it doesn't depend on system libc, so it expects `/etc/resolv.conf` file to exist.
         If the system doesn't have this file, use IP addresses instead of DNS names,
         e.g. `-o 51.89.217.80:9999` instead of `-o xmrpool.eu:9999`

[0]: https://gitlab.com/Kanedias/xmrig-static/-/releases/permalink/latest
[1]: https://gitlab.com/Kanedias/xmrig-static/-/releases/permalink/latest/downloads/xmrig-aarch64-static
[2]: https://gitlab.com/Kanedias/xmrig-static/-/releases/permalink/latest/downloads/xmrig-armv7-static
[3]: https://gitlab.com/Kanedias/xmrig-static/-/releases/permalink/latest/downloads/xmrig-x86_64-static
[4]: https://developer.android.com/tools/adb
